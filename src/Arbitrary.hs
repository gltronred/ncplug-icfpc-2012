module Arbitrary (emptyWorld, genSimpleWorld, addFlood, addTrampolines, addBeardsRazors, addHORocks, Arbitrary) where

import Types
import Geometry
import Engine
import Stringify

import Control.Monad
import qualified Data.Map as M
import Test.QuickCheck

atUniqueCoords n m w f = do
  x <- elements [2..n+1]
  y <- elements [2..m+1]
  let c = Coords x y
  if getItemAt w c == Empty
    then f c
    else atUniqueCoords n m w f
  

updateWorldWith :: Int -> Int -> Int -> WorldItem -> World -> Gen World
updateWorldWith 0 n m item w = return w
updateWorldWith count n m item w = atUniqueCoords n m w $ \c -> updateWorldWith (count-1) n m item $ w { worldMap = setItemAt (worldMap w) c item }

moveRobot n m w = atUniqueCoords n m w $ \c -> return w {robotPosition = c}

addWalls :: Int -> Int -> World -> Gen World
addWalls n m w = let
  wm0 = worldMap w
  walls = [(x,0) | x<-[0..n+1]] ++ [(x,m+1) | x<-[0..n+1]] ++ [(0,y) | y<-[1..m]] ++ [(n+1,y) | y<-[1..m]]
  size = 2*n + 2*m + 4
  in do
    pos <- elements [0..size-1]
    let wallItems = replicate pos Wall ++ [Lift] ++ repeat Wall
        wallsWithItems = zip walls wallItems
    return $ w { worldMap = foldr (\((x,y),i) wm -> setItemAt wm (Coords x y) i) wm0 wallsWithItems }

genSimpleWorld :: Int -> Int -> Double -> Double -> Double -> Double -> Gen World
genSimpleWorld minX minY freqWall freqRock freqLambda freqEarth = do
  n <- arbitrary`suchThat`(>=minX) :: Gen Int
  m <- arbitrary`suchThat`(>=minY) :: Gen Int
  world0 <- addWalls n m emptyWorld
  let size = n*m
      world = snd $ normalizeWorldTTM ((M.empty,M.empty), world0)
      getN freq = floor $ freq * fromIntegral size
      nWall = getN freqWall
      nRock = getN freqRock
      nLambda = getN freqLambda
      nEarth = getN freqEarth
  updateWorldWith nWall n m Wall world >>=
    updateWorldWith nRock n m (Rock False False) >>=
    updateWorldWith nLambda n m Lambda >>= return . (\w -> w { allLambdas = nLambda }) >>=
    updateWorldWith nEarth n m Earth >>=
    moveRobot n m

addFlood :: World -> Gen World
addFlood w = do
  wat <- arbitrary `suchThat` (>=0)
  flood <- arbitrary `suchThat` (>=0)
  proof <- arbitrary `suchThat` (>=0)
  return w { water = wat
           , flooding = flood
           , waterproof = proof }

addTrampolines :: Int -> World -> Gen World
addTrampolines count world = liftM fst $ foldM addTrampoline (world,0) [0..count-1]

addTrampoline :: (World, Int) -> Int -> Gen (World, Int)
addTrampoline wst@(w, sources) i = let
  dig = "123456789"!!i
  (Coords n m) = getWorldSize w
  in atUniqueCoords n m w $ \c -> addSources n m $ addTarget c dig wst

addSources :: Int -> Int -> (World, Coords, Int) -> Gen (World, Int)
addSources n m (world, c, s) = do
  cnt <- elements $ filter (<=9-s) $ 0:replicate 20 1 ++ [2..9]
  newWorld <- foldM (\w i -> atUniqueCoords n m w $ \cs -> return $ addSource cs (s+i) c w) world [0..cnt-1]
  return (newWorld, s+cnt)

addSource cs i c w = w { worldMap = setItemAt (worldMap w) cs $ Trampoline ch c }
  where ch = "ABCDEFGHI"!!i
addTarget c dig (w,s) = (w { worldMap = setItemAt (worldMap w) c $ Target dig }, c, s)

addBeardsRazors :: Double -> Int -> World -> Gen World
addBeardsRazors freqBeards razorsTotal world = let
  (Coords n m) = getWorldSize world
  size = n*m
  nBeards = floor $ freqBeards * fromIntegral size
  in do
    nRazors <- elements [0..razorsTotal]
    updateWorldWith nBeards n m Beard world { razors = razorsTotal - nRazors } >>=
      updateWorldWith nRazors n m Razor

addHORocks :: Double -> World -> Gen World
addHORocks freqHORocks world = let
  (Coords n m) = getWorldSize world
  size = n*m
  nHORocks = floor $ freqHORocks * fromIntegral size
  in updateWorldWith nHORocks n m (Rock True False) world

instance Arbitrary Move where
  arbitrary = elements $ enumFrom GoUp

instance Arbitrary World where
  arbitrary = genSimpleWorld 10 10 0.1 0.1 0.1 0.1 >>= addFlood >>= addTrampolines 3 >>= addBeardsRazors 0.03 3 >>= addHORocks 0.03