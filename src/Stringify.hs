{-# LANGUAGE BangPatterns #-}
-- Purpose: serializing / deserializing of the world
-- Author: Dmitry Vyal
module Stringify(worldToString, worldToString', stringToWorld, pathToString, normalizeWorldTTM) where

import Types

import Geometry

import Data.Maybe
import Data.Tuple

import qualified Data.Map as M
import Data.Map(Map,(!))

-- Map related

-- prints a world to string, only map part
worldToString' :: World -> String
worldToString' w = unlines rows
 where
   wm = worldMap w
   (b_min, b_max) = get_bounds w
   rows = map render_row [yCoord b_max, yCoord b_max - 1 .. yCoord b_min] -- bigger indices at top

   render_row y = drawWater y $ map (\x -> render_char (Coords x y)) [xCoord b_min .. xCoord b_max]
   render_char coords = let
     item = getItemAt w coords
     item' = case item of
               Trampoline c _ -> Trampoline c (Coords 0 0) -- Make trampolines look the same, only one entry in charMap
               _ -> item
     isym = fromMaybe (error $ "Can't find item symbol in charMap for " ++ show item) .
               (\c -> if item' == Lift && allLambdas w <= collectedLambdas w then Just 'O' else c) .
               lookup item' $ charMap
     in if robotPosition w == coords then 'R' else isym
   drawWater y s = if water w >= y then concat ["W",s,"W"] else concat [" ",s," "]

-- Renders all the world state to string, map and meta
worldToString :: World -> String
worldToString w = map_str ++ "\n" ++ meta_str
    where map_str = worldToString' w
          meta_str = showMetaLines w

instance Show World where
  show = worldToString

showMetaLines w = unlines $ filter (not. null) $ water_block ++ tramp_block ++ beard_block
  where
    meta caption getter def = case (getter w) == def of
                                False -> caption ++ " " ++ show (getter w)
                                True -> ""
    water_block = [meta "Water" water 0,
                   meta "Flooding" flooding 0,
                   meta "Waterproof" waterproof 10]
    beard_block = [meta "Growth" growth 25,
                   meta "Razors" razors 0]
    tramp_block = map (\(Trampoline c target_coords) ->
                           "Trampoline " ++ [c] ++ " targets " ++ get_target_char (worldMap w ! target_coords))
                       trampolines
    trampolines = M.elems $ M.filter is_trampoline (worldMap w)
    get_target_char (Target c) = [c]
    get_target_char _ = error "get_target_char defined only for targets"
    is_trampoline (Trampoline _ _) = True
    is_trampoline _ = False


-- parses a map string to world
stringToWorld' :: String -> ((TTMap,World),String)
stringToWorld' = (\(wttm, rest) -> (normalizeWorldTTM wttm, rest)) . stringReader empty_ttmap empty_world (Coords 1 1)
  where empty_world = emptyWorld
        empty_ttmap = (M.empty, M.empty)

-- parses a string with world and metadata to World
stringToWorld :: String -> World
stringToWorld s = let
    (wttm, meta) = stringToWorld' s
  in addMetadata (lines meta) wttm

-- update given World with specified metadata
addMetadata :: [String] -> (TTMap, World) -> World
addMetadata metadata (ttmap, w) = foldl (addMetaLine ttmap) w metadata

-- update given World with specified line of metadata
addMetaLine _ w "" = w
addMetaLine (trampolines, targets) w s =
    case words s of
      [name, valString] -> let
         value = read valString :: Int -- TODO: change if we can have non-Int values
        in case name of
             "Water" -> w { water = value }
             "Flooding" -> w { flooding = value }
             "Waterproof" -> w { waterproof = value }
             "Growth" -> w { growth = value, beardsStatus = value-1 }
             "Razors" -> w { razors = value }
      ["Trampoline", [tramp], "targets", [tgt]] ->
          w { worldMap = M.insert (trampolines ! tramp) (Trampoline tramp (targets ! tgt)) (worldMap w) }

-- Trampoline-Target map
type TTMap = (Map Char Coords, Map Char Coords)
-- helper, builds the world by consuming the map string character by character
-- Trampolines and targets are accumulated for future use


stringReader :: TTMap -> World -> Coords -> String -> ((TTMap, World), String)
stringReader ttmap w _ [] = ((ttmap, w),[])
stringReader ttmap w (Coords 1 _) ('\n':meta) = ((ttmap, w),meta)
stringReader ttmap w (Coords 1 _) ('\r':'\n':meta) = ((ttmap, w),meta)
stringReader ttmap w (Coords x y) ('\r':'\n':cs) = stringReader ttmap w (Coords 1 (y-1)) cs -- win
stringReader ttmap w (Coords x y) ('\n':cs) = stringReader ttmap w (Coords 1 (y-1)) cs -- unix
stringReader ttmap w (Coords x y) ('\r':cs) = stringReader ttmap w (Coords 1 (y-1)) cs -- mac(?)
stringReader tts@(trampolines,targets) !w pos@(Coords x y) (c:cs) = let
    mitem = lookup c revCharMap
    w' = case mitem of
           Just item -> w { worldMap = setItemAt (worldMap w) pos item}
           Nothing -> case c of
                        'R' -> w { robotPosition = pos}
                        _ -> error $ "can't lookup symbol " ++ [c]
    w'' = if mitem == Just Lambda || mitem == Just (Rock True False) then w' {allLambdas = allLambdas w + 1 } else w'
    tts' = case mitem of
             Just (Trampoline _ _) -> (M.insert c pos trampolines, targets)
             Just (Target _)       -> (trampolines, M.insert c pos targets)
             _ -> tts
  in stringReader tts' w'' (Coords (x+1) y) cs

normalizeWorldTTM ((trampolines, targets), w) = (ttmap', w' { worldBounds = get_bounds' $ worldMap w' })
  where w' = w { worldMap = M.mapKeys norm wm,
                                   robotPosition = norm $ robotPosition w }
        wm = worldMap w
        min_bound = fst $ get_bounds' wm
        norm c = c - min_bound + Coords 1 1
        ttmap' = (M.map norm trampolines, M.map norm targets)

-- map legend
charMap = [ (Lift, 'L')
          , (Rock False False, '*')
          , (Rock True False, '@')
          , (Rock False True, '*')
          , (Rock True True, '@')
          , (Wall, '#')
          , (Lambda, '\\')
          , (Earth, '.')
          , (Empty, ' ')
          , (Beard, 'W')
          , (Razor, '!')
          ] ++ map (\c -> (Trampoline c (Coords 0 0), c)) ['A'..'I'] ++ map (\c -> (Target c, c)) ['1'..'9']


revCharMap = map swap charMap

-- Move related

pathToString = map (\m -> fromMaybe (error "can't print such move") $ lookup m movesMap)

movesMap = [ (GoUp, 'U')
           , (GoDown, 'D')
           , (GoRight, 'R')
           , (GoLeft, 'L')
           , (Shave, 'S')
           , (Wait, 'W')
           , (Abort, 'A')]
