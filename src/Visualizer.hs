module Visualizer(visualizePaths, runMovesFromHandler, runMovesFromList, draw_turn,
                  run_sample_game)
where

import Types
import Stringify
import Engine

import System.IO
import System.FilePath

import Control.Concurrent
import Control.Monad(when)
import Data.List

import Data.Time.Clock

-- Play a game on a test map with a given number
run_sample_game :: Int -> IO ()
run_sample_game n = do
  let file = "test" </> "SampleMaps" </> ("contest" ++ show n ++ ".map")
  smap <- readFile file
  let w = stringToWorld smap
  runMovesFromHandler stdin w

draw_turn :: (World, MoveResult) -> IO ()
draw_turn ws@(world,status) = do
  putStrLn $ worldToString' world

  putStrLn $ "Score: " ++ show (scores ws)
  putStrLn $ "water: " ++ show (water world) ++ "; flooding: " ++ show (flooding world) ++ "; steps: " ++ show (stepsAfterRise world) ++ "; waterproof: " ++ show (waterproof world) ++ "; steps under water: " ++ show (stepsUnderWater world) ++ "; razors: " ++ show (razors world)
  case status of
    Aborted -> putStrLn "Exit: aborted"
    Win -> putStrLn "Exit: win"
    Destroyed -> do
      putStrLn $ worldToString' world
      putStrLn "Exit: destroyed"
    _ -> return ()


type MoveGen s = (s, RevHist)  -> IO (Maybe (Move, (s, RevHist)))
type RevHist = Path -- path with reversed order of moves
empty_hist = []

-- main visualization routines
runMovesFromHandler :: Handle -> World -> IO ()
runMovesFromHandler hMoves = do
  run_iterator (handler_gen hMoves) ((), empty_hist)

runMovesFromList :: Path -> World -> IO ()
runMovesFromList path = run_iterator list_gen (path, [])

slowdown = 200000
-- generator which grabs moves from handler
handler_gen :: Handle -> MoveGen ()
handler_gen hMoves = \(_, hist) -> do
  time0 <- getCurrentTime
  eof <- hIsEOF hMoves
  time1 <- getCurrentTime
  case eof of
    True -> do putStrLn "Exit: EOF"
               return Nothing
    False -> do
      moveChar <- hGetChar hMoves

      let move = case moveChar of
            'U' -> GoUp
            '8' -> GoUp
            'D' -> GoDown
            '2' -> GoDown
            'L' -> GoLeft
            '4' -> GoLeft
            'R' -> GoRight
            '6' -> GoRight
            'A' -> Abort
            '0' -> Abort
            'S' -> Shave
            '5' -> Shave
            _ -> Wait
      when (diffUTCTime time1 time0 < 0.01) $ threadDelay slowdown -- slow playback, it's probably not an interactive session
      return $ Just (move, ((), move:hist))

-- generator which plays moves from list
list_gen :: MoveGen Path
list_gen (move:rest, hist)  = do threadDelay slowdown
                                 return $ Just (move, (rest, move:hist))
list_gen ([], _) = return Nothing

-- move iteration routine, takes move generator, it's state and initial world, visualizes the game
run_iterator :: MoveGen s -> (s, RevHist) -> World -> IO ()
run_iterator gen state0 world0 = do
  draw_turn (world0, ContinueGame)
  vis_iterate gen state0 world0

-- helper
vis_iterate :: MoveGen s -> (s, RevHist) -> World -> IO ()
vis_iterate gen gen_state@(_,hist) world = do
  mg <- gen gen_state
  case mg of
    Nothing -> do putStr "Move history1: "
                  putStrLn $ pathToString (reverse hist)
    Just (move, gen_state'@(_, hist')) -> do
      let ws@(world', status') = makeFullMove world move
      putStrLn $ "> " ++ (show move) ++ "\n"
      draw_turn ws
      case status' of
        ContinueGame -> vis_iterate gen gen_state' world'
        _ -> do putStr "Move history2: "
                putStrLn $ pathToString (reverse hist')

-- Visualizer of a search process
visualizePaths :: World -> [Path] -> IO ()
visualizePaths _ [] = return ()
visualizePaths w0 (path:rest) = do
  putStrLn $ "Path: " ++ pathToString path
  runMovesFromList path w0
  putStrLn "**************** Path finished"
  visualizeDiffs w0 path rest

visualizeDiffs :: World -> Path -> [Path] -> IO ()
visualizeDiffs w0 old_path [] = return ()
visualizeDiffs w0 old_path (path:rest) = do
  let (common, postfix1, postfix2) = findCommon old_path path
      rev_game_states = reverse $ accumAll (makeFullMove . fst) (w0, ContinueGame) old_path
      (rev_postfix1, rev_common@((w1,s1):_)) = splitAt (length postfix1) $ rev_game_states
  putStrLn $ "*** New solution, rolling back " ++ show (length postfix1) ++ " moves"
  mapM_ (\gs -> draw_turn gs >> threadDelay (slowdown `div` 3)) $ drop 1 rev_postfix1
  return ()
  putStrLn "*** Go forward"
  putStrLn $ "Path: " ++ pathToString path
  run_iterator list_gen (postfix2, reverse common) w1
  visualizeDiffs w0 path rest

accumAll f x0 [] = [x0]
accumAll f x0 (y:ys) = x0 : accumAll f (f x0 y) ys

findCommon path1 path2 = findCommon' path1 path2 []
findCommon' [] ps2 comm = (reverse comm, [], ps2)
findCommon' ps1 [] comm = (reverse comm, ps1, [])
findCommon' path1@(p1:ps1) path2@(p2:ps2) comm =
    case p1 == p2 of
      True -> findCommon' ps1 ps2 (p1:comm)
      False -> (reverse comm, path1, path2)
