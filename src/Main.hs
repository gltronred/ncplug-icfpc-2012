module Main where

import Types
import Stringify

import Visualizer

import Searcher
import Heuristics

import System.Environment
import System.IO
import System.Exit

main = do
  args <- getArgs
  case args of
    [] -> driver getContents run_searcher
    ("play":map_file:rest) -> driver (readFile map_file) (interactive_play rest)
    ["search", map_file] -> driver (readFile map_file) debug_searcher
    ["vis-search", map_file] -> driver (readFile map_file) vis_debug_searcher
    _ -> do mapM_ putStrLn ["USAGE: lifter",
                            "lifter play <map file> [<moves file>]",
                            "lifter search <map>",
                            "lifter vis-search <map>"]
            exitFailure

driver :: IO String -> (World -> IO ()) -> IO ()
driver map_act worker = do
  smap <- map_act
  let world = stringToWorld smap
  worker world


submission = do
  smap <- getContents
  let world = stringToWorld smap
  run_searcher world

-- A variant for judges
-- silent worker
run_searcher world = mapM_ print_solution solutions
    where
      solutions = searcher world
      print_solution s = do
        putStrLn $ pathToString (getMovesFromStream s)
        hFlush stdout

-- MODES FOR DEBUGGING AND DEVELOPING

-- an interactive game
interactive_play files = \world -> do
  moveFileHandle <- if null files then return stdin else openFile (head files) ReadMode
  hSetEcho moveFileHandle False
  hSetBuffering moveFileHandle NoBuffering
  runMovesFromHandler moveFileHandle world

-- a test of search
debug_searcher = \world -> do
  let stream = searcher world
  mapM_ print_state stream
 where print_state s = do
          putStrLn $ "Evaluated Score: " ++ show (getScoreFromStream s)
          putStrLn $ "Estimated score: " ++ show (getEstScoreFromStream s)
          putStrLn $ "Moves: " ++ show (reverse $ getMovesFromStream s)
          draw_turn $ getWorldStateFromStream s

vis_debug_searcher = \world -> do
  let stream = searcher world
  visualizePaths world $ map getMovesFromStream stream

-- Current searcher
searcher = lifterBFS null_heuro -- stressLoad_heuro