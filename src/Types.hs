{-# LANGUAGE BangPatterns, DeriveGeneric #-}
-- Author - YanTayga
module Types
where

import qualified Data.Map as M
import GHC.Generics
import Data.Serialize
import Data.Serialize.Put

import Data.Digest.CRC32
import Data.Word

-- World data types

data WorldItem = Lift | Rock Bool Bool | Wall | Lambda | Earth | Empty | Trampoline !Char !Coords | Target !Char | Beard | Razor deriving (Eq, Show, Generic)

instance Serialize WorldItem where

data Coords = Coords {xCoord :: !Int,  yCoord :: !Int} deriving (Eq, Show, Generic)

instance Ord Coords where
  compare (Coords x1 y1) (Coords x2 y2)
    | y1 == y2 = compare x1 x2
    | otherwise = compare y1 y2

instance Serialize Coords where

type WorldMap = M.Map Coords WorldItem

data World = World {
  worldMap :: !WorldMap,    -- the world
  worldBounds :: (Coords,Coords),
  robotPosition :: !Coords, -- the current robot position
  allLambdas :: !Int,       -- how many lambdas was in initial position (unchangeable)
  collectedLambdas :: !Int, -- how many lambdas gathered
  moves :: !Int,            -- moves made by robot
  water :: !Int,            -- water level (default: 0)
  flooding :: !Int,         -- how many steps of map update phase before water level rises by 1; 0 -- no rising (default: 0)
  stepsAfterRise :: !Int,   -- how many steps have been done after last water rise
  waterproof :: !Int,       -- how many steps robot can be underwater (default: 10)
  stepsUnderWater :: !Int,  -- how many consecutive steps robot is under water
  beardsStatus :: !Int,     -- g in Wadler's beard. NB: Now (2012-07-15 13:31) it is common for all beards on map
  growth :: !Int,           -- Wadler's beard growth rate
  razors :: !Int            -- Hutton's razors count
  } deriving (Eq, Generic)

instance Serialize World where

-- Route data types

data Move = GoUp | GoDown | GoRight | GoLeft | Shave | Wait | Abort deriving (Eq, Show, Enum, Generic)

instance Serialize Move

type Path = [Move]

data MoveResult = ContinueGame | Aborted | Win | Destroyed deriving (Eq, Show, Generic)

instance Serialize MoveResult where

type WorldHash = Int

worldHash :: (World, MoveResult) -> WorldHash
worldHash (ws, gs) = fromIntegral $ crc32 $ runPutLazy $ put (ws{moves = 0}, gs)

-- Some trivial world specific tests
type CellTest = World -> Coords -> Bool

-- Clear point
clear :: WorldMap -> Coords -> WorldMap
clear !wm !c = M.delete c wm

-- Set item at point
setItemAt :: WorldMap -> Coords -> WorldItem -> WorldMap
setItemAt !wm !c Empty = clear wm c
setItemAt !wm !c !i    = M.insert c i wm

-- initial empty World
emptyWorld = World { worldMap = M.empty
                   , worldBounds = (Coords 0 0, Coords 0 0)
                   , robotPosition = Coords 0 0
                   , allLambdas = 0
                   , collectedLambdas = 0
                   , moves = 0
                   , water = 0
                   , flooding = 0
                   , stepsAfterRise = 0
                   , waterproof = 10
                   , stepsUnderWater = 0
                   , beardsStatus = 24
                   , growth = 25
                   , razors = 0 }

