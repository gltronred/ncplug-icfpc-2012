-- Purpose: Various Search heuristics to try...
-- Author: Dmitry Vyal

module Heuristics where

import qualified Data.Map as M
import Data.Maybe

import Types
import Geometry
import Engine
import Searcher

import Data.Digest.CRC32 (crc32)
import Data.List
import Data.Serialize (runPutLazy,put)

import Debug.Trace

null_heuro = \(rev_path, mov_res@(world, result)) -> scores mov_res


-- Blocked lift sucks

halfAround r = map (destination r) [GoUp, GoDown, GoRight, GoLeft]

movesDestination r cs = foldl' destination r cs
nMoves r k = concatMap (\i -> [movesDestination r $ repl i GoUp ++ repl (k-abs i) GoRight, movesDestination r $ repl i GoUp ++ repl (abs i - k) GoRight]) [-k..k]
  where repl i m | i<0 = replicate i $ rev m
        repl i m | otherwise = replicate i m

liftIsBlocked :: World -> Bool
liftIsBlocked w = if null l
                    then False
                    else all (not.isEmpty w) $ halfAround $ fst $ head l
  where 
    wm = worldMap w
    l = M.toList $ M.filter (== Lift) wm

lift_heuro = \(rev_path, mov_res@(world, result)) -> if liftIsBlocked world then 0 else scores mov_res


rev GoLeft = GoRight
rev GoRight = GoLeft
rev GoUp = GoDown
rev GoDown = GoUp

------------
-- Have to consider:
-- - lambdas
-- - water
-- - lift
-- - beard

isFree w c = if c == robotPosition w
             then True
             else let
               item = getItemAt w c
               in case item of
                 Empty -> True
                 Earth -> True
                 Lambda -> True
                 Trampoline _ _ -> True
                 Razor -> True
                 _ -> False

-- can find also falling rocks
isRockInState w s c = case getItemAt w c of
  Rock _ s' -> if isJust s then fromJust s==s' else True
  _ -> False

-- are we blocked
cannotMove w = let
  wm = worldMap w
  r = robotPosition w
  left x = destination x GoLeft
  right x = destination x GoRight
  in not (isAvailable w r || isRockInState w Nothing (left r) && isEmpty w (left $ left r) || isRockInState w Nothing (right r) && isEmpty w (right $ right r)) || willFallToMe w r

-- is there some way?
-- not exact
isAvailable w c = any (isFree w) (halfAround c) && not (fallenWillBlock w c)

willFallToMe w c = all (not.isFree w) [left,right] && isEmpty w down && isRock w up
  where [left,up,right,down] = map (destination c) [GoLeft,GoUp,GoRight,GoDown]

fallenWillBlock w r@(Coords x y) = let
  (falling,_) = M.findMin $ M.filterWithKey (\c i -> isRockInState w (Just True) c && xCoord c == x && yCoord c == y ) $ worldMap w
  [left,right,down] = map (destination r) [GoLeft,GoRight,GoDown]
  atop = map (movesDestination r) $ scanl (flip (:)) [GoUp] $ replicate (yCoord falling-y-2) GoUp
  in all (not.isFree w) [left,right,down] && all (isEmpty w) atop

-- is our position safe and how much?
-- cf. map 8
positionSafety w c@(Coords x y) = let
  (Coords n m) = getWorldSize w
  wm = worldMap w
  topList = dropWhile (\j -> isEmpty w (Coords x j)) [y+1..m]
  top = if null topList then y else head topList
  in case getItemAt w (Coords x top) of
    Rock _ _ -> -1000+10*(top-y)
    _ -> 1000

-- go like a snake to find a way and use it as heuristic to go. Too slow :(
-- maybe we should search only for first path (to do so change fixBest)
snakeWay w c = let
  r = robotPosition w
  d = distance r c
  depth = fromIntegral $ d + if d < 10 then 2*d else if d<20 then d else d`div`2
  isVeryFree w c = getItemAt w c == Earth || getItemAt w c == Lambda || getItemAt w c == Razor || isEmpty w c
  moveGen p = map (:p) $ filter (\m -> isFree (fst $ applyMoves p w) (destination r m) && (null p || rev m /= head p)) [GoLeft, GoDown, GoRight, GoUp]
  breadcrumbs = fixBest depth (distance c.movesDestination r.snd) $ bestFirstSearch (fromIntegral.crc32.runPutLazy.put) moveGen (distance c . movesDestination r) []
  best = last breadcrumbs
  estimate = if null breadcrumbs then 0 else fst best
  conventional = distance r c
  walkDist = conventional*conventional
  in if estimate < walkDist
     then distance c $ movesDestination r $ snd $ snd best
     else 4*conventional

snakeDist w a b = snakeWay w{robotPosition=a} b

-- distance to some object. We can use trampolines to get somewhere
-- now it uses only one trampoline (so we cannot use paths like  r -> A -> 1 -> B -> 2 -> c)
pathFinder w c = let
  wm = worldMap w
  r = robotPosition w
  tramps = M.mapWithKey (\source tr@(Trampoline ch target) -> snakeDist w r source + snakeDist w target c) $ M.filterWithKey (\c _ -> isTrampoline w c) wm
  trDist = if M.null tramps then 1000000 else snd $ M.findMin tramps
  -- fixDistance d = if d < 5 then d*4 else if d < 10 then d*3 else if d < 20 then d*2 else d
  in -- fixDistance $
     min trDist $ snakeDist w r c

stressLoad_heuro (revPath, movRes@(w, result)) = let
  wm = worldMap w
  (Coords n m) = getWorldSize w
  r = robotPosition w
  -- We are interested in Lambdas, Lifts and Razors only now
  interesting = M.filter (\a -> a==Lambda || a==Rock True False || a==Lift || a==Razor) wm
  -- Find nearest interesting object and go. Lift should be considered interesting only if all lambdas collected
  nearestL = (\(c,(m,i)) -> (c,i)) $ M.findMax $ M.mapWithKey (\c i -> if i/=Lift then (markLambda c i,i) else (-10000,i)) interesting
  beards = M.size $ M.filter (\a -> a==Beard) wm
  markLambda c@(Coords x y) i = if isAvailable w c then (if i==Razor then 500 else if i==Rock True False then 150 else 350) - pathFinder w c + (if flooding w > 0 then water w + 5*flooding w - y else -y*10`div`m) else -5000
  -- if we stop, we will have 50 for each collected lambda
  estScore = 150 * collectedLambdas w + razors w
  meHalfBlocked = if cannotMove w then 1 else 0
  -- if we have negative mark of interesting object, it is Open Lift and we have to go there
  noNegative x = if x< -9999 then 0 else x
  movingHORs = M.filter (\i -> i==Rock True True) wm
  in if result /= ContinueGame || result /= Aborted
     then scores movRes
     else estScore - 150*meHalfBlocked - 10 * beards - length revPath + noNegative (uncurry markLambda nearestL) + 50*M.size movingHORs

-- NOTE: Do not work
-- Go to a safe position. Collect lambdas and wait somewhere in safe place while rocks are falling
-- Attempt to solve (make better solution) of 3 and 8
coward_heuro (revPath, movRes@(w,result)) = let
  wm = worldMap w
  r = robotPosition w
  estScore = 500 * collectedLambdas w
  interesting = M.filter (\a -> a==Lambda || a==Lift) wm  
  nearestL = fst $ M.findMax $ M.mapWithKey (\c i -> if i==Lambda then markLambda c else -10000) interesting
  markLambda c@(Coords x y) = if isFree w c then 200*distance r c + 10*(water w + 5*flooding w - y) - y*1000 else -10000  
  safety = positionSafety w r
  moving = M.filter (\a -> a==Rock True True || a==Rock False True) wm
  in 1000 + estScore + safety {- - M.foldrWithKey (\c _ m -> m + 10*distance r c) 0 moving -} + markLambda nearestL

-- Try to find such positions and solve them:
-- *****#
-- \\\\\
--     \
--     \
lambdaLine_heuro (revPath, movRes@(w,result)) = let
  wm = worldMap w
  r = robotPosition w
  estScore = 500 * collectedLambdas w
  interesting = M.filter (\a -> a==Lambda || a==Lift) wm
  in 0