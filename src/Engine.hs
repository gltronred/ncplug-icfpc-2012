{-# LANGUAGE BangPatterns, ViewPatterns #-}
-- Authors: YanTayga, Mansur
module Engine
where

import qualified Data.Map as M
import Debug.Trace
import Stringify

import Types
import Geometry

destination :: Coords -> Move -> Coords
destination c Wait = c
destination c Abort = c
destination c Shave = c
destination (Coords x y) GoUp = Coords x (y + 1)
destination (Coords x y) GoDown = Coords x (y - 1)
destination (Coords x y) GoLeft = Coords (x - 1) y
destination (Coords x y) GoRight = Coords (x + 1) y

-- First point is strictly one pos above second point
above :: Coords -> Coords -> Bool
above (Coords x1 y1) (Coords x2 y2) = y1 - y2 == 1 && x1 == x2

addMoveCount !w = let
  mv = moves w
  wat = water w
  flood = flooding w
  after = stepsAfterRise w
  under = stepsUnderWater w
  !wat' = wat + (if flood /= 0 && after == flood-1 then 1 else 0) -- flooding == 0 means no flooding
  !after' = if flood == 0 || after == flood-1 then 0 else after + 1 -- let after remains 0 when there are no flooding
  !under' = if yCoord (robotPosition w) <= wat' then under+1 else 0
  in w{moves = mv+1, water = wat', stepsAfterRise = after', stepsUnderWater = under'}

updateBeardGrowth (!w,!res) = (w { beardsStatus = if beardsStatus w == 0 then growth w - 1 else beardsStatus w - 1 }, res)

moveRobotTo !r !w = w{robotPosition = r}

around r = map (destination r) [GoUp, GoDown, GoRight, GoLeft] ++ concatMap (\m -> map (destination $ destination r m) [GoUp, GoDown]) [GoLeft, GoRight]

clearBeards w = let
  wm = worldMap w
  r = robotPosition w
  clearBeard c wm = if getItemAt w c == Beard
                    then clear wm c
                    else wm
  in w { worldMap = foldr clearBeard wm $ around r, razors = razors w - 1 }

-- make 1st move step and checking win and aborting conditions
makeMove :: World -> Move -> (World, MoveResult)
makeMove !w Abort = (w, Aborted)
makeMove !w Wait = (addMoveCount w, ContinueGame)
makeMove !w Shave = if razors w > 0 then (addMoveCount $ clearBeards w, ContinueGame) else (addMoveCount w, ContinueGame)
makeMove !w !m = makeMoveOnItem $! getItemAt w c
  where
    wm = worldMap w
    cl = collectedLambdas w
    al = allLambdas w
    r = robotPosition w
    c = destination r m
    cn = if m == GoLeft || m == GoRight then destination c m else c
    mc = moves w
    rz = razors w
    nonChanged = (addMoveCount w, ContinueGame)
    oneMove r  = (addMoveCount $ moveRobotTo c $ w { worldMap = clear wm c }, r)

    makeMoveOnItem Wall             = nonChanged
    makeMoveOnItem Lift             = if al == cl then oneMove Win else nonChanged
    makeMoveOnItem Empty            = oneMove ContinueGame
    makeMoveOnItem Earth            = oneMove ContinueGame
    makeMoveOnItem Lambda           = (addMoveCount $ moveRobotTo c $ w { worldMap = clear wm c, collectedLambdas = 1 + cl}, ContinueGame)
    makeMoveOnItem r@(Rock b _)     = if isEmpty w cn
      then (addMoveCount$ moveRobotTo c $ w { worldMap = clear (setItemAt wm cn r) c }, ContinueGame)
      else nonChanged
    makeMoveOnItem Beard            = nonChanged
    makeMoveOnItem Razor            = (addMoveCount $ moveRobotTo c $ w { worldMap = clear wm c, razors = 1 + rz}, ContinueGame)
    makeMoveOnItem (Target _)       = nonChanged
    makeMoveOnItem (Trampoline _ t) = case getItemAt w t of
      (Target _) -> (addMoveCount $ moveRobotTo t $ w { worldMap = clear (clearTrampolines wm t) t}, ContinueGame)
      _          -> (addMoveCount $ moveRobotTo t $ w { worldMap = clear wm c }, ContinueGame)

-- Removes all trampolines pointing to a given cell
clearTrampolines :: WorldMap -> Coords -> WorldMap
clearTrampolines wm target = M.filter (\item -> case item of Trampoline _ target' -> target /= target'; _ -> True) wm

-- make 2 and 3 move step - rocks are rollings and checking loose conditions
afterMove :: World -> (World, MoveResult)
afterMove !w = isRobotDestroyedUnderwater $! updateBeardGrowth $ M.foldrWithKey' updateRock (w, ContinueGame) wm
  where
    wm = worldMap w
    robot = robotPosition w

    updateRock !r rock@(Rock b s) res@(!w', ContinueGame) =
      case getItemAt w down of
        Empty  -> if robotPosition w /= down
                    then (w'{worldMap = (setItemAt (setItemAt (worldMap w') down $ Rock b True) r Empty)}, continueStatus down robot)
                    else stopRock res
        Rock b' s' -> if isEmpty w right && isEmpty w rightdown
                    then (w'{worldMap = (setItemAt (setItemAt (worldMap w') rightdown $ conv rock) r Empty)}, continueStatus rightdown robot)
                    else
                      if isEmpty w left && isEmpty w leftdown
                        then (w'{worldMap = (setItemAt (setItemAt (worldMap w') leftdown $ conv rock) r Empty)}, continueStatus leftdown robot)
                        else stopRock res
        Lambda -> if isEmpty w right && isEmpty w rightdown
                    then (w'{worldMap = (setItemAt (setItemAt (worldMap w') rightdown $ conv rock) r Empty)}, continueStatus rightdown robot)
                    else stopRock res
        _ -> stopRock res
      where
        down = destination r GoDown
        right = destination r GoRight
        left = destination r GoLeft
        rightdown = destination (destination r GoRight) GoDown
        leftdown = destination (destination r GoLeft) GoDown
        continueStatus !rock !robot = if above rock robot then Destroyed else ContinueGame
        conv (Rock True _) = Lambda
        conv (Rock False _) = Rock False False
        stopRock res = if s then (w'{worldMap = setItemAt (worldMap w') r $ conv rock}, ContinueGame) else res
    updateRock !r Beard res@(!w',ContinueGame) = let
      wm0 = worldMap w'
      in if beardsStatus w' == 0
         then (w' {worldMap = foldr (\c wm -> if isEmpty w c then setItemAt wm c Beard else wm) wm0 $ around r}, ContinueGame)
         else res
    updateRock _ _ res = res

    isRobotDestroyedUnderwater (!w,!s) = if stepsUnderWater w > waterproof w then (w,Destroyed) else (w,s)

-- Make a full turn, move robot and process the environment
makeFullMove !world !move = let
    (!world1, !res1) = makeMove world move
    !result = case res1 of
      ContinueGame -> afterMove world1
      _ -> (world1, res1)
    in result

scores :: (World, MoveResult) -> Int
scores (w, res) = lambdaAmount res * collectedLambdas w - moves w
  where
    lambdaAmount Aborted = 50
    lambdaAmount Win     = 75
    lambdaAmount _       = 25


applyMoves :: Path -> World -> (World, MoveResult)
applyMoves [] world = (world, ContinueGame)
applyMoves (step:path) world = let
  (world',res) = makeFullMove world step
  in case res of
    ContinueGame -> applyMoves path world'
    _ -> (world',res)