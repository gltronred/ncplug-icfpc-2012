-- Purpose: Various geometry related primitives and utilites, not physics
-- Author: Dmitry Vyal
module Geometry where

import Types

import qualified Data.Map as M

instance Num Coords where
    (+) = zipCoordsWith (+)
    negate (Coords x1 y1) = Coords (-x1) (-y1)
    (*) = error "* didn't defined for Coords"
    abs = error "abs didn't defined for Coords"
    signum = error "signum didn't defined for Coords"
    fromInteger n = Coords (fromIntegral n) (fromIntegral n)

-- returns (top left, bottom right) corners of a map
get_bounds' :: M.Map Coords a -> (Coords, Coords)
get_bounds' m = M.foldWithKey (\ c _ (c_min, c_max) ->
                              (zipCoordsWith min c c_min, zipCoordsWith max c c_max)
                             ) (init_coord,init_coord) m
 where init_coord = case M.keys m of
                      (k:_) -> k
                      [] -> Coords 1 1 -- Avoid failing on empty map

get_bounds w = worldBounds w

-- apply a given function to coord
zipCoordsWith f (Coords x1 y1) (Coords x2 y2) = Coords (f x1 x2) (f y1 y2)

distance c1 c2 = (\(Coords x y) -> abs x + abs y) $ zipCoordsWith (-) c1 c2

getWorldSize w = uncurry (zipCoordsWith $ flip (-)) $ get_bounds w


-- Point is empty?
isEmpty :: CellTest
isEmpty w c@(Coords x y)  = let
  (Coords n m) = getWorldSize w
  in if x<=0 || y<=0 || x>n+1 || y>m+1
     then False
     else
       case getItemAt w c of
       Empty -> robotPosition w /= c
       _ -> False


-- isTrampoline here?
isTrampoline :: CellTest
isTrampoline w c = case getItemAt w c of
                     Trampoline _ _ -> True
                     _ -> False

isRock :: CellTest
isRock w c = case getItemAt w c of
               Rock _ _ -> True
               _ -> False

isOpenLift w c = case getItemAt w c of
                   Lift -> cl == al
    where
      cl = collectedLambdas w
      al = allLambdas w


-- Get item at point
getItemAt :: World -> Coords -> WorldItem
getItemAt w c@(Coords x y) = let
  (Coords n m) = getWorldSize w
  in if x<=0 || y<=0 || x>n+1 || y>m+1
     then Wall
     else case (M.lookup c $ worldMap w) of
                  Nothing -> Empty
                  Just x -> x
