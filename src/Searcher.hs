{-# LANGUAGE FlexibleContexts, ScopedTypeVariables  #-}
-- Purpose: searches through game tree
-- Author: Dmitry Vyal
module Searcher(lifterBFS, getMovesFromStream, getScoreFromStream, getEstScoreFromStream, getWorldStateFromStream, Score, EstScore, SearchStream, lifterBFSDepth, fixBest, bestFirstSearch) where

import Types
import Geometry
import Engine

import qualified Data.Heap as H
import Data.List
import qualified Data.IntSet as S
import qualified Data.Map as M

import Debug.Trace

type Score = Int -- Precise score, for selecting the best solution
type EstScore = Int -- Estimated score, for guiding the search
type Hash = Int

-- walks the search space, generates all possible moves in best first order
bestFirstSearch :: forall s. (H.HeapItem H.FstMaxPolicy (EstScore, s)) => (s -> Hash) -> (s -> [s]) -> (s -> EstScore) -> s -> [(EstScore, s)]
bestFirstSearch hasher move_generator evaluator start_state =
      bsf S.empty (H.fromAscList [(evaluator start_state, start_state)] :: H.MaxPrioHeap EstScore s)
  where bsf visited_states solutions =
          case H.view solutions of
            Nothing -> []
            Just (best, rest) -> let
              hash = hasher $ snd best
              visited_states' = S.insert hash visited_states
              child_states = move_generator $ snd best
              scores = map evaluator child_states
              solutions' = foldl' (flip H.insert) rest $ zip scores child_states
             in case S.notMember hash visited_states of
                  True -> best : bsf visited_states' solutions' -- output new state and take children into account
                  False -> bsf visited_states rest -- discarding computation, make use of laziness

-- fixes the best result so far
-- Negative depth means infinity
fixBest :: Integer -> (s -> Score) -> [s] -> [(Score,s)]
fixBest maxDepth evaluator solution_stream@(s:ss) = fix' 0 s (evaluator s) ss
    where
      fix' step best_state best_score (cur:rest) | maxDepth < 0 || step < maxDepth =
                 let cur_score = evaluator cur
                     step' = step + 1
                 in case cur_score >= best_score of
                     True -> (cur_score, cur) : fix' step' cur cur_score rest
                     _ -> fix' step' best_state best_score rest -- or keep steps count the same?
      fix' _ _ _ _ = []

-- A BFS with separated fitness functions for state generation (estimator) and result evaluation (evaluator) phases
heuroBFS :: Integer -> (s -> Hash) -> (s -> [s]) -> (s -> Score) -> (s -> EstScore) -> s -> [(Score, (EstScore, s))]
heuroBFS depth hasher move_generator evaluator estimator start_state = fixBest depth (evaluator . snd) stream
    where stream = bestFirstSearch hasher move_generator estimator start_state

-- Lifter specific solvers

lifterBFSDepth :: Integer -> ((Path, (World, MoveResult)) -> EstScore) -> World -> [(EstScore, (Score, (Path, (World, MoveResult))))]
lifterBFSDepth depth estimator w = heuroBFS depth hasher generator (scores . snd) estimator init_state
    where generator (rev_path, (world, result)) = case result of
            ContinueGame -> map (\m -> (m:rev_path, makeFullMove world m)) $ select_good_moves world all_moves
            _ -> []
          hasher (rev_path, (world, result)) = worldHash (world, result)
          init_state = ([], (w, ContinueGame))

-- Rejecting stupid moves
select_good_moves w [] = []
select_good_moves w (m:ms) = if good then m:rest else rest
            where good = case m of
                    GoUp -> can_move
                    GoDown -> can_move && not (isRock w up) -- can't kill us
                    GoRight -> can_move || isRock w dest && isEmpty w (destination dest GoRight) --FIXME test fo kill
                    GoLeft -> can_move || isRock w dest && isEmpty w (destination dest GoLeft) --FIXME test fo kill
                    Shave -> can_wait && razors w > 0 && any (\c -> getItemAt w c == Beard) (around place)
                    Wait -> can_wait
                    Abort -> True
                  can_wait = hasActiveEls && not (isEmpty w up && isRock w (destination up GoUp)) -- can't be killed
                  can_move = canMoveTo w dest
                  place = robotPosition w
                  dest = destination place m
                  up = destination place GoUp
                  wm = worldMap w
                  rest = select_good_moves w ms
                  hasActiveEls = True -- not $ M.null $ M.filter (\i -> i == Rock True True || i == Rock False True) $ worldMap w

canMoveTo :: World -> Coords -> Bool
canMoveTo w c = not $ case getItemAt w c of
                   Lift -> (collectedLambdas w /= allLambdas w)
                   Rock _ _ -> True
                   Wall -> True
                   Target _ -> True
                   Beard -> True
                   _ -> False

type SearchStream = [(EstScore, (Score, (Path, (World, MoveResult))))]

lifterBFS :: ((Path, (World, MoveResult)) -> EstScore) -> World -> SearchStream
lifterBFS = lifterBFSDepth (-1)

-- A bunch of accessor functions
getMovesFromStream (est_score, (score, (rev_moves, world_state))) = reverse rev_moves
getScoreFromStream (est_score, (score, (rev_moves, world_state))) = score
getEstScoreFromStream (est_score, (score, (rev_moves, world_state))) = est_score
getWorldStateFromStream (est_score, (score, (rev_moves, world_state))) = world_state

all_moves = enumFrom $ toEnum 0

