module Main where

import Test.HUnit
import System.FilePath
import System.IO (stdout)
import System.Exit (exitFailure, exitSuccess)

-- import tests
import EngineTests

import Stringify


sampleMaps = map (("test" </> "SampleMaps") </>) . map (\n -> "contest" ++ show n ++ ".map") $ [1..37]

-- parse, print, parse and compare results of parses.
-- String representations may differ a little, but worlds must be the same
checkStringifier str = let
    w = stringToWorld str
    str2 = worldToString w
    w2 = stringToWorld str2
  in w == w2

mapTests = map (\file -> " On map " ++ takeFileName file ~: readFile file >>= \s -> (checkStringifier s) @? "checkStringifier failed\n\n" ++ s) sampleMaps


myRunTestTT t = do (counts, 0) <- runTestText (putTextToHandle stdout True) t
                   return $ (errors counts) + (failures counts)

runAllTests = myRunTestTT $ test $ concat [ mapTests, [engineTests] ]

main = do
     nerr <- runAllTests
     if nerr /= 0
        then exitFailure
        else exitSuccess

