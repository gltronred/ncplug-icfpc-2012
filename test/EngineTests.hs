module EngineTests where

import Test.HUnit

import Types
import Stringify
import Engine


-- During map update, all reads are from the old state, and all writes are to the new state. This can mean that rocks sometimes crash into each other.
testRocksCrash = let
  mapStartStr   = "#* *#\n\
                  \#* *#\n\
                  \#####\n"
  worldStart    = stringToWorld mapStartStr
  mapResultStr  = "#   #\n\
                  \#***#\n\
                  \#####\n"
  worldResult   = stringToWorld mapResultStr
  worldComputed = fst $ afterMove worldStart
  in "Rocks crash:" ~: (worldMap worldResult) ~=? (worldMap worldComputed)

testMoves = let
  testMap0 = "#####\n#   #\n# R #\n#   #\n#####\n"
  testMapD = "#####\n#   #\n#   #\n# R #\n#####\n"
  testMapU = "#####\n# R #\n#   #\n#   #\n#####\n"
  testMapR = "#####\n#   #\n#  R#\n#   #\n#####\n"
  testMapL = "#####\n#   #\n#R  #\n#   #\n#####\n"

  testIn = map stringToWorld [testMap0, testMap0, testMap0, testMap0, testMap0, testMap0]

  testMove = [GoUp, GoDown, GoRight, GoLeft, Wait, Abort]

  testOut1 = [((stringToWorld testMapU){moves = 1}, ContinueGame), ((stringToWorld testMapD){moves = 1}, ContinueGame), ((stringToWorld testMapR){moves = 1}, ContinueGame), ((stringToWorld testMapL){moves = 1}, ContinueGame), ((stringToWorld testMap0){moves = 1}, ContinueGame), ((stringToWorld testMap0){moves = 0}, Aborted)]

  in "Moves:" ~: zipWith makeMove testIn testMove ~=? testOut1

testWater = let
  testMapIn = "#####\n#   #\n# R #\n#   #\n#####\n\nWater 0\nFlooding 5\nWaterproof 3\n"

  testMoves1 = [Wait, Wait, Wait, Wait, Wait]

  testMoves2 = testMoves1 ++ testMoves1

  testOut1 = foldr (flip (makeFullMove.fst)) (stringToWorld testMapIn, ContinueGame) testMoves1

  testOut2 = foldr (flip (makeFullMove.fst)) (stringToWorld testMapIn, ContinueGame) testMoves2

  in "Moves:" ~: [water $ fst testOut1, water $ fst testOut2] ~=? [1, 2]

testUnderwater = let
  testMapIn = "#####\n#   #\n#   #\n# R #\n#####\n\nWater 0\nFlooding 5\nWaterproof 3\n"

  testMoves = replicate 12 Wait

  testOut1 = foldr (flip (makeFullMove.fst)) (stringToWorld testMapIn, ContinueGame) testMoves

  testOut2 = (makeFullMove $ fst testOut1) Wait

  in "Moves underwater:" ~: and [(stepsUnderWater $ fst testOut1) == 3, (stepsUnderWater $ fst testOut2) == 4, snd testOut1 == ContinueGame, snd testOut2 == Destroyed] ~=? True

testUnderwater2 = let
  testMapIn = "#####\n#   #\n#   #\n# R #\n#####\n\nWater 0\nFlooding 5\nWaterproof 5\n"

  testMoves = replicate 14 Wait

  testOut1 = foldr (flip (makeFullMove.fst)) (stringToWorld testMapIn, ContinueGame) testMoves

  testOut2 = (makeFullMove $ fst testOut1) GoUp

  in "Stay underwater:" ~: [snd testOut1, snd testOut2] ~=? [ContinueGame, Destroyed]

testTrampoline = let
  testMapIn = "#####\n#   #\n# 1B#\n#AR #\n#####\n\nTrampoline A targets 1\nTrampoline B targets 1\n"

  testMoves = [GoLeft, GoRight]

  testOut = foldr (flip (makeFullMove.fst)) (stringToWorld testMapIn, ContinueGame) testMoves

  in "Trampolines:" ~: robotPosition (fst testOut) ~=? (Coords 4 3)

testBeards = let
  testMapIn = stringToWorld "#####\n#   #\n# W #\n#   #\n#R  #\n###L#\n\nGrowth 2\nRazors 1"

  testMoves1 = [Wait, Wait]
  testMoves2 = [Wait, Wait, Wait, Wait]
  testMoves3 = [Wait, Wait, Shave]

  expected1 = stringToWorld "#####\n#WWW#\n#WWW#\n#WWW#\n#R  #\n###L#\n\nGrowth 2\nRazors 1"
  expected2 = stringToWorld "#####\n#WWW#\n#WWW#\n#WWW#\n#RWW#\n###L#\n\nGrowth 2\nRazors 1"
  expected3 = stringToWorld "#####\n#WWW#\n#WWW#\n#  W#\n#R  #\n###L#\n\nGrowth 2"

  testOut1 = fst $ applyMoves testMoves1 testMapIn
  testOut2 = fst $ applyMoves testMoves2 testMapIn
  testOut3 = fst $ applyMoves testMoves3 testMapIn
  in "Beards:" ~: TestList [ (worldMap testOut1) ~=? (worldMap expected1)
                           , (worldMap testOut2) ~=? (worldMap expected2)
                           , (worldMap testOut3) ~=? (worldMap expected3)
                           , (razors testOut3) ~=? 0 ]

testBeards2 = let
  testMapIn = stringToWorld "#######\n#     #\n#     #\n#  W  #\n#     #\n#     #\n#R    #\n#####L#\n\nGrowth 2\nRazors 1"
  testMoves = replicate 4 Wait
  testOut = fst $ applyMoves testMoves testMapIn
  expected = stringToWorld "#######\n#WWWWW#\n#WWWWW#\n#WWWWW#\n#WWWWW#\n#WWWWW#\n#R    #\n#####L#\n\nGrowth 2\nRazors 1"
  in "Beards big:" ~: (worldMap testOut) ~=? (worldMap expected)

engineTests = "Engine tests:" ~: test [ testMoves, testRocksCrash, testWater, testUnderwater, testUnderwater2, testTrampoline, testBeards, testBeards2 ]
