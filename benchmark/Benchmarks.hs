{-# LANGUAGE BangPatterns #-}
module Main where

import System.FilePath
import System.IO (stdout)
import System.Exit (exitFailure, exitSuccess)
import System.Environment
import Data.Time.Clock

import Types
import Stringify
import Searcher
import Heuristics


data BenchStats = BenchStats { minTime, maxTime, avgTime, sumTime :: NominalDiffTime,
                               minScore, maxScore, avgScore, sumScore :: Score,
                               minEst, maxEst, avgEst, sumEst :: EstScore }

sampleMaps = map (("test" </> "SampleMaps") </>) . map (\n -> "contest" ++ show n ++ ".map") $ [1..37]


searcher0 maxDepth = lifterBFSDepth maxDepth null_heuro
searcher1 maxDepth = lifterBFSDepth maxDepth stressLoad_heuro

searchers = [("null_heuro", searcher0), ("stressLoad_heuro", searcher1)]


benchSearcherMap :: Integer -> (Integer -> World -> [(EstScore, (Score, (Path, (World, MoveResult))))]) -> World -> IO (NominalDiffTime, Score, EstScore)
benchSearcherMap maxDepth searcher world = do
    start <- getCurrentTime
    let stream = searcher maxDepth world
        (estScore, (score, (path, (_, moveRes)))) = last $ (-1,(-1,([Abort],(world,Aborted)))): stream
    stop <- score `seq` estScore `seq` moveRes `seq` getCurrentTime
    putStr $ show $ diffUTCTime stop start ; putStr ", "
    putStr $ show score ; putStr ", "
    putStr $ show estScore ; putStr ", "
    putStr $ show $ length path ; putStr ", "
    putStrLn $ show moveRes
    return ((diffUTCTime stop start), score, estScore)

benchMap :: Integer  -> World -> IO [(NominalDiffTime, Score, EstScore)]
benchMap maxDepth world = mapM ( \(title, searcher) -> do
  putStr "  " ; putStr title ; putStr ": "
  benchSearcherMap maxDepth searcher world ) searchers

collectBenchStats maxDepth maps = collectBenchStats' maxDepth zeros 0 maps
  where
    zeros = replicate (length searchers) $
        BenchStats { minTime = fromInteger 1000000, maxTime = fromInteger 0, avgTime = fromInteger 0, sumTime = fromInteger 0,
                     minScore = 1000000, maxScore = 0, avgScore = 0, sumScore = 0,
                     minEst = 1000000, maxEst = 0, avgEst = 0, sumEst = 0 }
    collectBenchStats' :: Integer -> [BenchStats] -> Int -> [FilePath] -> IO [BenchStats]
    collectBenchStats' maxDepth bss steps maps = if null maps
      then return bss
      else do
           putStr (head maps); putStrLn ": "
           mapStr <- readFile (head maps)
           let m = stringToWorld mapStr
           statsList <- length (worldToString m) `seq` benchMap maxDepth m
           let !bss' = updateStatsList bss statsList
           collectBenchStats' maxDepth bss' (steps + 1) (tail maps)
      where
        updateStatsList = zipWith updateStats
        updateStats bs (time, score, est) = 
          BenchStats { minTime = min (minTime bs) time, maxTime = max (maxTime bs) time, avgTime = ((avgTime bs)*(fromInteger $ toInteger steps) + time) / (fromRational . toRational $ steps+1), sumTime = (sumTime bs) + time,
                       minScore = min (minScore bs) score, maxScore = max (maxScore bs) score, avgScore = ((avgScore bs)*steps + score) `div` (steps+1), sumScore = (sumScore bs) + score,
                       minEst = min (minEst bs) est, maxEst = max (maxEst bs) est, avgEst = ((avgEst bs)*steps + est) `div` (steps+1), sumEst = (sumEst bs) + est }

main = do
     env <- getEnvironment
     let maxDepth = case lookup "SEARCH_DEPTH" env of
                        Just s -> read s
                        Nothing -> 1000000 :: Integer -- let it be default for benchmarking

     putStrLn "Stats: Map: Heuristic: Time, Score, EstimatedScore, Path length, Result"
     statsList <- collectBenchStats maxDepth sampleMaps
     putStrLn "Collected stats: min, max, avg, sum"
     mapM_ printStats $ zipWith (\(heuro, _) stats -> (heuro, stats)) searchers statsList
  where
    printStats (heuro, stats) = do
      putStr heuro ; putStrLn ":"
      putStrLn $ "  Time: " ++ (show $ minTime stats) ++ ", " ++ (show $ maxTime stats) ++ ", " ++ (show $ avgTime stats) ++ ", " ++ (show $ sumTime stats)
      putStrLn $ "  Score: " ++ (show $ minScore stats) ++ ", " ++ (show $ maxScore stats) ++ ", " ++ (show $ avgScore stats) ++ ", " ++ (show $ sumScore stats)
      putStrLn $ "  Estimate: " ++ (show $ minEst stats) ++ ", " ++ (show $ maxEst stats) ++ ", " ++ (show $ avgEst stats) ++ ", " ++ (show $ sumEst stats)

